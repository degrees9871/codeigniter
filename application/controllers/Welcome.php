<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{


    public function index()
    {
        $this->load->helper('url');
        $this->load->model('Comments');
        $this->load->library("pagination");

        $config = [];


        $config['full_tag_open'] = "<ul class='pagination justify-content-center'>";
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['attributes'] = ['class' => 'page-link'];
        $config['next_link'] = 'Next Page';
        $config['prev_link'] = 'Previous Page';
        $config['num_links']= 5;


        $config["base_url"] = base_url();
        $config["total_rows"] = $this->Comments->get_count();
        $config["per_page"] = 2;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(1)) ? $this->uri->segment(1) : 0;

        $data["links"] = $this->pagination->create_links();

        $data['comments'] = $this->Comments->get_comments($config["per_page"], $page);


        $this->load->view('welcome_message', $data);


    }

    public function insert()
    {

        $this->load->helper('url');
        $this->load->model('Comments');
        $postData = $this->input->post();

        if (!empty($postData['email'] && !empty($postData['message']))) {
            if (empty($postData['name'])) {
                $postData['name'] = strtok( $postData['email'], '@');
            }
            $this->Comments->insert_comments($postData['name'],$postData['email'],$postData['message']);
        }



        redirect('welcome/index');
    }
}
