<?php

class Comments extends CI_Model {

    protected $table = 'comments';

    public function __construct() {
        parent::__construct();
    }

    public function get_count() {
        return $this->db->count_all($this->table);
    }

    public function get_comments($limit, $start) {
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get($this->table);

        return $query->result();
    }

    public function insert_comments($name, $email, $message) {
        $data = [
            'nickname' => $name,
            'email' => $email,
            'comment' => $message
        ];

        $this->db->insert($this->table, $data);
    }
}