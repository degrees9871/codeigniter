-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/

-- Время создания: Авг 31 2020 г., 18:04
-- Версия сервера: 10.0.33-MariaDB
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `comments`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comment` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `nickname`, `email`, `comment`) VALUES
(1, 'Jack', 'jackblack@gmail.com', 'Перед початком нового навчального року керівники закладів освіти Києва в онлайн-форматі презентували повну готовність шкіл до зустрічі дітей. Вхід до закладів обмежений, тому батьки можуть упевнитися в дотриманні всіх необхідних карантинних вимог завдяки детальним віртуальним екскурсіям.'),
(2, 'Bill', 'billmurray@horizon.net', '«Ми вперше зустрічаємо навчальний вересень у незвичних умовах. Діти приходитимуть до школи за зміненими графіками, в закладах відкрито додаткові входи, розроблені напрямки руху учнів, на стінах та підлозі розміщені відповідні вказівники та маркери дистанціювання. Ідея директорів – заспокоїти батьків і показати, що освіта столиці «озброєна», заклади здійснили підготовчі заходи та отримали дезінфікуючі засоби в повному обсязі», – зазначив в.о. першого заступника голови КМДА Валентин Мондриївський.'),
(3, 'Ben', 'benaffleck@cpanel.net', 'Ініціювали відео-флешмоб освітяни Дніпровського району. У своїх відеороликах директори презентують оновлений формат роботи закладів і розповідають про:'),
(4, 'John', 'johntravolta@ex.ua', 'час початку уроків для початкової, середньої та старшої школи;\n-температурний скринінг на вході;\n-забезпечення закладів дезінфікуючими засобами;\n-провітрювання та вологе прибирання в кабінетах під час перерв;\n-заняття на свіжому повітрі за індивідуальним графіком;\n-закріплення кабінетів за класами;\n-соціальне дистанціювання та розмітки руху.'),
(5, 'Bill', 'billgates@microsoft95.net', '«Здоров’я дітей – це відповідальність усіх учасників освітнього процесу. Ми просимо батьків бути максимально уважними, відповідально дотримуватися правил гігієни, слідкувати за станом здоров’я дитини та вимірювати температуру перед виходом до школи кожного дня», – додав Мондриївський.'),
(6, 'Donald Trump', 'duck@gmaul.com', 'Hello'),
(7, 'david', 'david@david.beckham', 'Playing fifa 99\r\n'),
(8, 'ze', 'ze@gov.ua', 'зе!'),
(9, 'тест', 'test@test.test', 'тест');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
